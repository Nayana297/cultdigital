@extends('layouts.app')

@section('content')
<div class="page-wrapper">
    <!-- Page Content -->
    <div class="content container-fluid">

        <!-- Page Header -->
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col">
                    <h3 class="page-title">Books Management System</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li class="breadcrumb-item active">Books</li>
                    </ul>
                </div>
                <div class="col-auto float-right ml-auto">
                    <a href="#" class="btn add-btn" data-toggle="modal" data-target="#add_modal" id="add_dep_btn"><i class="fa fa-plus"></i> Add Book</a>
                </div>
            </div>
        </div>
        <!-- /Page Header -->

        <div class="row">
            <div class="col-md-12">
                <div>
                    <table class="table table-striped table-bordered" id="books_tbl" style="table-layout: fixed;">
                        <thead>
                            <th class="text-center" style="width: 30px;">#</th>
                            <th class="text-center">Book name</th>
                            <th class="text-center">Category</th>
                            <th class="text-center">Author</th>
                            <th class="text-center">published date</th>
                            <th class="text-center">image </th>
                            <th class="text-center">Action </th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content -->

    <!-- Add Book Modal -->
    <div id="add_modal" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><label id="header_label">Add Book</label></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="book_form" method="post" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" id="book_id" name="book_id" value="">

                        <div class="col-sm-6 imgUp">
                            <label class="btn btn-primary">Upload<input name="book_image" id="book_image" type="file" class="uploadFile img" value="Upload Photo" style="width: 0px;height: 0px;overflow: hidden;" accept="image/*"  title="accept :jpg,jpeg,png  max file size :5MB">
                            </label>
                            <span class="text-danger error-text book_image_err"></span>
                        </div>

                        <div class="form-group">
                            <label>Book Name <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" id="book_name" name="book_name">
                            <span class="text-danger error-text book_name_err"></span>
                        </div>

                        <div class="form-group">
                            <label>Category <span class="text-danger">*</span></label>
                            <select data-live-search="true" class="form-control selectpicker" id="select_category" name="select_category">
                                <option value="">Select Category</option>
                                @foreach ($categories as $category) 
                                    <option value="{{$category->id }}">{{$category->cate_name }}</option>
                                @endforeach 
                               
                            </select>
                             <span class="text-danger error-text select_category_err"></span>
                        </div>

                        <div class="form-group">
                            <label>Author <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" id="author_name" name="author_name">
                            <span class="text-danger error-text author_name_err"></span>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Published date <span class="text-danger">*</span></label>
                            <div class="cal-icon"><input data-date-format="yyyy-mm-dd" id="published_date" name="published_date" class="form-control datepicker"> </div>
                            <span class="text-danger error-text published_date_err"></span>
                        </div>

                        <div class="submit-section">
                            <button class="btn btn-primary submit-btn" id="submit">Submit</button>
                            <button class="btn btn-primary submit-btn" id="update" style="display: none;">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- /Add Book Modal -->

    <!-- Delete Book Modal -->
    <div class="modal custom-modal fade" id="delete_modal" role="dialog">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="form-header">
                        <h3>Delete Book</h3>
                        <p>Are you sure want to delete?</p>
                    </div>
                    <div class="modal-btn delete-action">
                         <form  id='bookdata' method="post" class="form-horizontal">
                            {{ csrf_field() }}
                            <input type="hidden" name="_method" value="PUT">
                            <input type="hidden" id="book_id" name="book_id" value="">
                            <div class="row">
                                <div class="col-6">
                                    <a type="submit" class="btn btn-primary continue-btn" id="delete_btn">Delete</a>
                                </div>
                                <div class="col-6">
                                    <a data-dismiss="modal" class="btn btn-primary cancel-btn">Cancel</a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Delete Department Modal -->
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        fetchAllBooks();

        $('.selectpicker').selectpicker();

        $('#published_date').datepicker({
            autoclose: true,
            todayHighlight: true,
        });

        $('#book_form').submit(function(e){
            e.preventDefault();     
            var formData = new FormData(this);
     
            $.ajax({
                url: 'book',
                type:'POST',
                data: formData,
                contentType: false,
                processData: false,
                success: function(data) {
                    printMsg(data);
                    if($.isEmptyObject(data.error)){
                        $('#add_modal').modal('hide');
                        $('#book_form')[0].reset();
                        fetchAllBooks();
                    }
                }
            });
        }); 
    });

    $('body').on('click', '#editBook', function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        fetchEditData(id);
    });

    $('body').on('click', '#deleteBook', function (event) {
        var id = $(this).data('id');
        $('#book_id').val(id);
        $('#delete_modal').modal('show'); 
    });

    $('body').on('click', '#delete_btn', function (event) {
        event.preventDefault();
        var id     = $("#book_id").val();
        var _token = $("input[name='_token']").val();

        $.ajax({
              url: 'book/'+id,
              type: 'DELETE',
              data: {
                id: id,
                _token:_token,
            },
            success: function (response){
                printMsg(response); 
                $('#delete_modal').modal('hide');       
            }
         });
          return false;
    });

    $('body').on('click', '#update', function (event) {
        event.preventDefault();

        var form     = $('#book_form')[0];
        var formData = new FormData(form);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: 'POST',
            url: '/book/' + $("#book_form input[name=book_id]").val(),
            data: formData,
            processData: false,  // Important!
            contentType: false,
            cache: false,
            success: function(data) {
                printMsg(data);
                if($.isEmptyObject(data.error)){
                   $('#add_modal').modal('hide');
                    $('#book_form')[0].reset();
                    fetchAllBooks();
                }
            }
        });
    });

    function fetchEditData(id)
    {   
        $.get('book/' + id + '/edit', function (data) {
            $('#add_modal').modal('show');
            $('#book_id').val(data.id);

            $('#submit').hide();
            $('#update').show();
            $('#header_label').text('Edit Book');

            $('#book_name').val(data.b_name);
            $('#select_category').val(data.category_id);
            $('#author_name').val(data.author_name);
            $('#published_date').val(data.publish_date);

            $('.selectpicker').selectpicker('refresh');;
        })
    }

    function fetchAllBooks()
    {
       $.ajax({
           method: 'GET',
           url: "{{ route('books.index') }}",
           success: function(books)
           { 
                var table;
                $('#books_tbl').dataTable().fnClearTable();
                $('#books_tbl').dataTable().fnDestroy();

                table =  $('#books_tbl').DataTable(
                {
                  "columnDefs": [
                    {"className": "dt-center", "targets": "_all"}
                ]}); 

                if(books!='') { 
                    $.each(books, function(i, item) {
                        table.row.add([ books[i].count,books[i].b_name,books[i].category, books[i].author_name, books[i].publish_date, books[i].image,books[i].action]);
                    });              
                }

                table.draw();         
            },
            error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log('comes error');
            }
        }); 
    }
    </script>
@endsection