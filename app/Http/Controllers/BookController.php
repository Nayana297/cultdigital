<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Book;
use App\Models\Category;
use URL;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories    = Category::OrderBy('created_at','asc')->get();  
        return view('home', ['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'book_name'      => 'required',
            'select_category' => 'required',
        ]);

        if ($validator->passes()) {

            if ($request->file('book_image')) {
                $imagePath = $request->file('book_image');
                $imageName  =   time().rand(1,988).'.'.substr(strrchr($imagePath->getClientOriginalName(),'.'),1);

                $path = $request->file('book_image')->storeAs('book_image', $imageName, 'public');
            }
            else
                $path =null;

            $book               = new Book;
            $book->b_name       = $request->book_name;
            $book->category_id  = $request->select_category;
            $book->b_image      = $path;
            $book->author_name  = $request->author_name;
            $book->publish_date = $request->published_date;
            $book->save();

            return response()->json(['success'=>'Book created successfully.']);
        }

        return response()->json(['error'=>$validator->errors()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Book = Book::find($id);
        return response()->json($Book);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'book_name'      => 'required',
            'select_category' => 'required',
        ]);

        if ($validator->passes()) {

            $path = null;

            if ($request->file('book_image')) {
                $imagePath = $request->file('book_image');
                $imageName  =   time().rand(1,988).'.'.substr(strrchr($imagePath->getClientOriginalName(),'.'),1);

                $path = $request->file('book_image')->storeAs('book_image', $imageName, 'public');
            }

            $book               = Book::find($id);
            $book->b_name       = $request->book_name;
            $book->category_id  = $request->select_category;

            if($path != null)
                $book->b_image = $path;

            $book->author_name  = $request->author_name;
            $book->publish_date = $request->published_date;
            $book->save();

            return response()->json(['success'=>'Book updated successfully.']);
        }

        return response()->json(['error'=>$validator->errors()]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        return response()->json([
          'success' => 'Book deleted!'
        ]);
    }

    function fetch_books(Request $request)
    {
        
        $books   = Book::with('category')->OrderBy('created_at','desc')->get();        
        $final_array   = array();
        $i             = 0;

        foreach ($books as $key => $book) 
        {
            $final_array[$key]['count']        = (++$i);
            $final_array[$key]['b_name']       = $book->b_name;
            $final_array[$key]['category']     = $book->category->cate_name;
            $final_array[$key]['author_name']  = $book->author_name;  
            $final_array[$key]['publish_date']        = $book->publish_date;

            $book_image ='';

            if($book->b_image !=NULL){
                $book_image = URL::asset('storage/'.$book->b_image) ;
                $final_array[$key]['image'] = '<img width="50%;" height="50%;" src="'.$book_image.'"/>';
            }
            else{

                $final_array[$key]['image'] = '';
            }

            $final_array[$key]['action']          = '<a title="Edit" href="" class="btn btn-primary" id="editBook" data-toggle="modal" data-target="#add_modal" data-id="' . $book->id . '">Edit</a> <a title="Delete" href="" class="btn btn-danger" id="deleteBook" data-target="#delete_modal" data-toggle="modal" data-id="' .$book->id . '">Delete</a>';
        }

        return response()->json($final_array);     
    }
}
